apiVersion: batch/v1
kind: Job
metadata:
  name: post-install-sso-config
  labels:
    # The "app.kubernetes.io/instance" convention makes it easy to tie a release to all of the
    # Kubernetes resources that were created as part of that release.
    app.kubernetes.io/instance: {{ .Release.Name }}
    # This makes it easy to audit chart usage.
    helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    app.kubernetes.io/name: post-install-grafana
  annotations:
    # This is what defines this resource as a hook. Without this line, the
    # job is considered part of the release.
    "helm.sh/hook": post-install
    "helm.sh/hook-delete-policy": "before-hook-creation,hook-succeeded"
spec:
  template:
    metadata:
      name: post-install-sso-config
      labels:
        app.kubernetes.io/instance: {{ .Release.Name }}
        app.kubernetes.io/name: post-install-sso-config
    spec:
      restartPolicy: OnFailure
      serviceAccount: tiller
      serviceAccountName: tiller
      containers:
        - name: post-install-sso-config
          image: "gitlab-registry.cern.ch/paas-tools/openshift-client"
          # We use tiller SA for deploying, it has configMap permissions
          # All we're going to do is sleep for a while, then exit.
          command:
            - bash
            - -c
            - |-
              # We add a delay on the job itself as we cannot specify it with Helm
              # This helps the ServiceInstance provisioning, as avoids to interfere if they are still running
              sleep 30s
              # Replace CERN SSO proxy configuration. We patch the existing configMap so it can be cleaned by Tiller.
              # Get the new Authorize configuration value
              # Workaround with backticks to avoid rendering with brackets. Ref https://github.com/helm/helm/issues/2798#issuecomment-467319526
              newAuthorizeConf=$(oc get configmap cern-sso-proxy-post-install -o go-template='{{`{{index .data "authorize.conf"}}{{println}}`}}')
              # overwrite the value with key "authorize.conf" with value from the temporary configMap
              patch=$(jq -n --arg newvalue "$newAuthorizeConf" '{"data":{"authorize.conf":$newvalue}}')
              oc patch configmap cern-sso-proxy -p "${patch}"
              # Get the new proxy configuration value
              # Workaround with backticks to avoid rendering with brackets. Ref https://github.com/helm/helm/issues/2798#issuecomment-467319526
              newProxyConf=$(oc get configmap cern-sso-proxy-post-install -o go-template='{{`{{index .data "proxy.conf"}}{{println}}`}}')
              # overwrite the value with key "authorize.conf" with value from the temporary configMap
              patch=$(jq -n --arg newvalue "$newProxyConf" '{"data":{"proxy.conf":$newvalue}}')
              oc patch configmap cern-sso-proxy -p "${patch}"
              # Delete temporary configMap
              oc delete cm cern-sso-proxy-post-install
              # To avoid discordances on the configuration we rollout SSO proxy, otherwise some endpoints might not be available
              oc rollout latest cern-sso-proxy
          volumeMounts:
          - mountPath: /tmp
            name: cern-sso-proxy-post-install
      volumes:
        - configMap:
            defaultMode: 420
            name: cern-sso-proxy-post-install
          name: cern-sso-proxy-post-install


