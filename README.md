# Draft deployment steps

## Prerequisites for Helm/Tiller

Start a Docker container for installation (mount the git repo in /project):

```bash
export DEPLOYMENT_NAMESPACE=<namespace>
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=${DEPLOYMENT_NAMESPACE} gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash

# login as cluster admin to the destination cluster
oc login https://openshift-dev.cern.ch
oc project $TILLER_NAMESPACE

# install tiller
export HELM_VERSION=2.13.1
curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv linux-amd64/helm /usr/bin/
```

1. Create namespace, prepare tiller permissions and required resources.

```bash
helm template /project/prerequisites --values /project/prerequisites/values.yaml | oc create -f -
```

2. Deploy tiller

```bash
helm init --service-account tiller
```

## Deployment

```bash
# Update Gitlab chart dependency! Otherwise Helm expects it to be present in /Charts
cd /project/gitlab-monitoring/
helm repo add gitlab https://charts.gitlab.io/
helm dependency update
# values.yaml already has the required configuration
# Specify Alertmanager email prefix, corresponding to the GitLab environment
export MAIL_PREFIX=GITLAB-PREFIX
export PROMETHEUS_WEBHOOK_PORT=4000
export DEPLOYMENT_NAMESPACE=$TILLER_NAMESPACE
envsubst < /project/gitlab-monitoring/values_template.yaml > /project/gitlab-monitoring/values.yaml
# Deploy all components  
helm --debug install --name gitlab-monitoring /project/gitlab-monitoring/ --timeout 600 -f /project/gitlab-monitoring/values.yaml
```

## Post-deployment

Grafana deployment still requires a missins step:

```bash
oc set env dc/grafana GF_SERVER_DOMAIN="${NAMESPACE}.web.cern.ch" GF_SERVER_ROOT_URL="%(protocol)s://%(domain)s:/grafana" -n $NAMESPACE GF_DATABASE_TYPE=mysql GF_DATABASE_HOST=${GRAFANA_DATABASE_HOST} GF_DATABASE_USER=${GRAFANA_DATABASE_USER} GF_DATABASE_PASSWORD=${GRAFANA_DATABASE_PASSWORD}
# Redeploy SSO proxy
oc rollout latest cern-sso-proxy
```

This project will be contained in the Gitlab namespace and will add an exporter to avoid joining with any other network.

## Uninstall

```bash
helm delete --purge gitlab-monitoring
helm template /project/prerequisites/ | oc delete -f -
```
